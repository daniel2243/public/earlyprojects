#include "Random.h"
#include <iostream>
#include <chrono>
#include <random>
#include <string_view>



class Game
{
private:
    static const int boardSize {4}; //trying to make adjustable board size, but it's annoying so let's see how long i bother doing it
    char board[boardSize][boardSize];
    int boardMin {0};
    int boardMax {boardSize-1};
    char currentPlayer {'X'};
    bool legalMove {false};
    bool gameOver {false};
    bool vsCpu {false};
    int movesMade{0};
    std::string_view horizontalEdge{ "------" };
    std::string_view boardSpaces{ "|     " };
public:
    Game();
    void newGame();
    void askIfVsCpu();
    void showBoard();
    void printBoardSpaces();
    void printHorizEdge();
    void moveFunction();
    bool checkWin();
    bool checkDraw();
    bool cpuBlock();
    bool inRange(int x);
    void swapPlayer();
    void winMessage();
    void play();
};

Game::Game()
{
    newGame();
}

void Game::newGame()
{
    currentPlayer = 'X';
    legalMove = false;
    gameOver = false;
    vsCpu = false;
    movesMade = 0;
    for (int row {0}; row < boardSize; ++row)
    {
        for (int col {0}; col < boardSize; ++col)
        {
            board[row][col] = ' ';
        }
    }
}

void Game::askIfVsCpu()
{
    std::cout << "\nWould you like to play vs the machine? (y/n): ";
    char chooseVsCpu {};
    std::cin >> chooseVsCpu;
    if (chooseVsCpu == 'Y' || chooseVsCpu == 'y')
    {
        vsCpu = true;
    }
    else
    {
        vsCpu = false;
    }
}

void Game::showBoard()
{
    std::cout << "\n\n\n";
    printHorizEdge();
    for (int row {0}; row < boardSize; ++row)
    {
        printBoardSpaces();
        for ( int col {0}; col < boardSize; ++col )
        {
            std::cout << "|  " << board[row][col] << "  " ;
        }
        std::cout << "|\n";
        printBoardSpaces();
        printHorizEdge();
    }
}

void Game::printBoardSpaces()
{
    for (int i {0}; i < boardSize; ++i)
        {
            std::cout << boardSpaces;
        }
    std::cout << "|\n";
}

void Game::printHorizEdge()
{
    
    for (int i {0}; i < boardSize; ++i)
    {
        std::cout << horizontalEdge;
    }
    std::cout << "-\n";
}

void Game::moveFunction()
{
    if (!vsCpu || currentPlayer == 'X')
    {
        std::cout << "Player " << currentPlayer << " enter square to play (1-" << boardSize * boardSize << "): ";
        int chosenMove {};
        std::cin >> chosenMove;
        if (chosenMove >= 1 && chosenMove <= boardSize*boardSize)
        {
            int translateChoice { chosenMove - 1 };
            int row { translateChoice / boardSize };
            int col { translateChoice % boardSize };
            if (board[row][col] == ' ')
            {
                board[row][col] = currentPlayer;
                legalMove = true;
            }
            else
            {
                std::cout << "That square is taken, please try again.\n";
            }
        }
        else
        {
            std::cout << "That's not on the board, please try again.\n";
        }
    }
    else 
    {
        if (cpuBlock() != true)
        {
            bool cpuLegal {false};
            int cpuMove {};
            while (!cpuLegal)
            {
                cpuMove = Random::get( 1, boardSize*boardSize );
                int traCpuMove { cpuMove - 1 };
                int row { traCpuMove / boardSize };
                int col { traCpuMove % boardSize };
                if (board[row][col] == ' ')
                {
                    board[row][col] = currentPlayer;
                    cpuLegal = true;
                }
            }
        }
        legalMove = true;
    }
}

bool Game::checkWin()
{
    int accountingForBoardSize {boardSize-3};

    for (int i {0}; i < boardSize; ++i) //rows
    {
        for (int j {0}; j <= accountingForBoardSize; ++j)
        {
            if (board[i][j] != ' ' && board[i][j] == board[i][j+1] && board[i][j] == board[i][j+2])
            {
                return true;
            } 
        }
    }

    for (int i {0}; i < boardSize; ++i) //columns
    {
        for (int j {0}; j <= accountingForBoardSize; ++j)
        {
            if (board[j][i] != ' ' && board[j][i] == board[j+1][i] && board[j][i] == board[j+2][i])
            {
                return true;
            } 
        }
    }


    for (int i {0}; i <= accountingForBoardSize; ++i) //top left to bottom right diagonals
    {
        for (int j {0}; j <= accountingForBoardSize; ++j)
        {
            if (board[i][j] != ' ' && board [i][j] == board[i+1][j+1] && board[i][j] == board[i+2][j+2])
            {
               return true;
            }
        }
    }

    for (int i {boardSize-1}; i >= 2; --i) //bottom left to top right diagonals
    {
           for (int j {0}; j <= accountingForBoardSize; ++j)
        {
            if (board[i][j] != ' ' && board[i][j] == board[i-1][j+1] && board[i][j] == board[i-2][j+2])
            {
               return true;
            }
        }
    }
    return false;
}

bool Game::checkDraw()
{
    if (movesMade >= boardSize*boardSize)
        return true;
    else
        return false;
}

bool Game::cpuBlock()
{
    int cBSize { boardSize-2 };

    for (int i {0}; i < boardSize; ++i) //rows
    {
        for (int j {0}; j <= cBSize; ++j)
        {
            if ( board[i][j] == 'X' && board[i][j] == board[i][j+1] )
            {
                if (inRange(j+2) && board[i][j+2] == ' ')
                {
                    board[i][j+2] = currentPlayer;
                    return true;
                }
                else if (inRange(j-1) && board[i][j-1] == ' ')
                {
                    board[i][j-1] = currentPlayer;
                    return true;
                }
            }
        }
    }


    for (int i {0}; i < boardSize; ++i) //columns
    {
        for (int j {0}; j <= cBSize; ++j)
        {
            if ( board[j][i] == 'X' && board[j][i] == board[j+1][i] )
            {
                if (inRange(j+2) && board[j+2][i] == ' ')
                {
                    board[j+2][i] = currentPlayer;
                    return true;
                }
                else if (inRange(j-1) && board[j-1][i] == ' ')
                {
                    board[j-1][i] = currentPlayer;
                    return true;
                }
            }
        }
    }

    for (int i {0}; i <= cBSize; ++i) //backslash diags
    {
        for (int j {0}; j <= cBSize; ++j)
        {
            if ( board[i][j] == 'X' &&  board[i][j] == board[i+1][j+1] )
            {
                if ( inRange(i+2) && inRange(j+2) && board[i+2][j+2] == ' ' )
                {
                    board[i+2][j+2] = currentPlayer;
                    return true;
                }
                else if ( inRange(i-1) && inRange(j-1) && board[i-1][j-1] == ' ')
                {
                    board[i-1][j-1] = currentPlayer;
                    return true;
                }
            }
        }
    }

    for (int i {1}; i < boardSize; ++i) //slash diags
    {
        for (int j {0}; j <= cBSize; ++j)
        {
            if (board[i][j] == 'X' && board[i][j] == board[i-1][j+1])
            {
                if (inRange(i-2) && inRange(j+2) && board[i-2][j+2] == ' ')
                {
                    board[i-2][j+2] = currentPlayer;
                    return true;
                }
                else if (inRange(i+1) && inRange(j-1) && board[i+1][j-1] == ' ')
                {
                    board[i+1][j-1] = currentPlayer;
                    return true;
                }
            }
        }
    }

    return false;
}

bool Game::inRange(int x)
{
    if (x >= boardMin && x <=boardMax)
        return true;
    else
        return false;
}

void Game::swapPlayer()
{
    if (currentPlayer == 'X')
    {
        currentPlayer = 'O';
    }
    else if (currentPlayer == 'O')
    {
        currentPlayer = 'X';
    }
}

void Game::winMessage()
{
    std::cout << "\nCongrats! Player " << currentPlayer << " has won!\n";
}

void Game::play()
{
    Game();
    askIfVsCpu();
    while (!gameOver)
    {
        showBoard();
        legalMove = false;
        while (!legalMove)
        {
            moveFunction();
        }
        if (checkWin())
        {
            gameOver = true;
        }
        else if (checkDraw())
        {
            gameOver = true;
        }
        else
        {
            swapPlayer();
        }
    }
    showBoard();
    winMessage();
}




int main()
{
    bool wantToPlay {true};
    while (wantToPlay)
    {
        Game test;
        test.play();
        std::cout << "Want to play again? (y/n) ";
        char playMore {};
        std::cin >> playMore;
        if (playMore == 'y' || playMore == 'Y')
            wantToPlay = true;
        else
            wantToPlay = false;
    }

    std::cout << "\n\n\n";
    system("pause");
    return 0;
}