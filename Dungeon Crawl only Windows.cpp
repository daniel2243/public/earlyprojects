#include "Random.h"
#include <chrono>
#include <conio.h>
#include <iostream>
#include <random>

class DungeonCrawl
{
private:
    static const int boardSize {10};
    static const int numEnemies {2};
    char board[boardSize][boardSize];
    char player {'P'};
    char trap {'T'};
    char enemy {'E'};
    char treasure {'X'};
    int numTraps {3};
    int enemyPos[2][numEnemies];
    int playerPosRow {1};
    int playerPosCol {1};
    int newPlayerPosRow{};
    int newPlayerPosCol{};
    bool validInput {false};
    bool gameOver {false};
public:
    DungeonCrawl();
    void newGame();
    void welcomMessage();
    void showBoard();
    void playerMove();
    void playerDirection();
    bool checkInBounds(int row, int col);
    void playerMeets(int row, int col);
    void enemyMove();
    bool checkEnemyInBounds(int who, int row, int col);
    bool checkNewEnemyPos(int who, int row, int col);
    void play();
};

DungeonCrawl::DungeonCrawl()
{
    newGame();
}

void DungeonCrawl::newGame()
{
    gameOver = false;
    for (int row {0}; row < boardSize; ++row)
    {
        for (int col {0}; col < boardSize; ++col)
        {
            board[row][col] = '-';
        }
    }

    playerPosRow = 1;
    playerPosCol = 1;

    board[playerPosRow][playerPosCol] = player;

    board[9][9] = treasure;

    for (int placeTraps {1}; placeTraps <= numTraps; ++placeTraps)
    {
        bool validTrap {false};
        while (!validTrap)
        {
            int ranNum { Random::get(0, boardSize*boardSize-1) };
            int ranNumToRow { ranNum / boardSize };
            int ranNumToCol { ranNum % boardSize };
            if ( board[ranNumToRow][ranNumToCol] == '-')
            {
                board[ranNumToRow][ranNumToCol] = trap;
                validTrap = true;
            }
        }
    }

    for (int placeEnemies {1}; placeEnemies <= numEnemies; ++placeEnemies)
    {
        bool validEnemy {false};
        while (!validEnemy)
        {
            int ranNum { Random::get(0, boardSize*boardSize-1) };
            int ranNumToRow { ranNum / boardSize };
            int ranNumToCol { ranNum % boardSize };
            if ( board[ranNumToRow][ranNumToCol] == '-')
            {
                board[ranNumToRow][ranNumToCol] = enemy;
                enemyPos[0][placeEnemies-1] = ranNumToRow;
                enemyPos[1][placeEnemies-1] = ranNumToCol;
                validEnemy = true;
            }
        }
    }
}

void DungeonCrawl::welcomMessage()
{
    std::cout << "\nWelcome to Dungeon Crawl. Reach the treasure while avoiding traps and enemies. \n" ;
    std::cout << "You are 'P', traps are 'T', enemies are 'E' and 'X' marks the treasure. Move with WASD. \n";
    std::cout << "Press any key to continue." << std::endl;
    char temp {};
    temp = _getch();
}

void DungeonCrawl::showBoard()
{
    std::cout << "\n\n\n\n";
    for (int row {0}; row < boardSize; ++row)
    {
        for (int col {0}; col < boardSize; ++col)
        {
            std::cout << " " << board[row][col];
        }
        std::cout << "\n";
    }
}

void DungeonCrawl::playerMove()
{
    std::cout << "\n\nMake your move. \n";
    playerDirection();
    playerMeets(newPlayerPosRow, newPlayerPosCol);

}

void DungeonCrawl::playerDirection()
{
    bool validInput = false;
    char playerInput {};
    while (!validInput)
    {
        playerInput = _getch();
        switch (playerInput)
        {
            case 'w':
            case 'W':
                if (checkInBounds(-1, 0) == true)
                    {
                        validInput = true;
                        return;
                    }
                else
                    break;
            case 'a':
            case 'A':
                if (checkInBounds(0, -1) == true)
                    {
                        validInput = true;
                        return;
                    }
                else
                    break;
            case 's':
            case 'S':
                if (checkInBounds(1, 0) == true)
                    {
                        validInput = true;
                        return;
                    }
                else
                    break;
            case 'd':
            case 'D':
                if (checkInBounds(0, 1) == true)
                    {
                        validInput = true;
                        return;
                    }
                else
                    break;
            default:
                std::cout << "\nInvalid input, please try again.";
        }
    }
}

bool DungeonCrawl::checkInBounds(int row, int col)
{
    newPlayerPosRow = playerPosRow+row;
    newPlayerPosCol = playerPosCol+col;
    if ( newPlayerPosRow >= 0 && newPlayerPosRow < boardSize && newPlayerPosCol >= 0 && newPlayerPosCol < boardSize )
        return true;
    else
    {
        std::cout << "\nThat's gonna put you outside the board, please try again.\n";
        return false;
    }
}

void DungeonCrawl::playerMeets(int row, int col)
{
    char newSpot {board[row][col]};
    switch (newSpot)
    {
        case '-':
            board[playerPosRow][playerPosCol] = '-';
            playerPosRow = row;
            playerPosCol = col;
            board[playerPosRow][playerPosCol] = player;
            return;
        case 'T':
            board[playerPosRow][playerPosCol] = '-';
            board[row][col] = 't';
            showBoard();
            std::cout << "\nYou hit a trap and die. Better luck next time! \n";
            gameOver = true;
            return;
        case 'E':
            board[playerPosRow][playerPosCol] = '-';
            board[row][col] = 'e';
            showBoard();
            std::cout << "\nYou hit an enemy and die. Better luck next time! \n";
            gameOver = true;
            return;
        case 'X':
            board[playerPosRow][playerPosCol] = '-';
            playerPosRow = newPlayerPosRow;
            playerPosCol = newPlayerPosCol;
            board[playerPosRow][playerPosCol] = 'p';
            showBoard();
            std::cout << "\nCongratulations, you got the treasure! \n";
            gameOver = true;
            return;
        default:
            std::cout << "\nProgrammer probably fucked up o.O\n";
            return;
    }
}

void DungeonCrawl::enemyMove()
{
    for (int enemyNum {0}; enemyNum < numEnemies; ++enemyNum)
    {
        if (!gameOver)
        {
            int validDir[4] {1, 1, 1, 1};
            bool enemyCanMove {false};
            while (enemyCanMove == false)
            {
                if (validDir[0] + validDir[1]  +validDir[2] + validDir[3] > 0)
                {
                    int ranDir { Random::get(1, 4) };
                    switch (ranDir)
                    {
                        case 1:
                            if (checkEnemyInBounds(enemyNum, -1, 0) == true)
                            {
                                enemyCanMove = true;
                                break;
                            }
                            else
                            {
                                validDir[0] = 0;
                                break;
                            }    
                        case 2:
                            if (checkEnemyInBounds(enemyNum, 0, -1) == true)
                            {
                                enemyCanMove = true;
                                break;
                            }
                            else
                            {
                                validDir[1] = 0;
                                break;
                            }
                        case 3:
                            if (checkEnemyInBounds(enemyNum, 1, 0) == true)
                            {
                                enemyCanMove = true;
                                break;
                            }
                            else
                            {
                                validDir[2] = 0;
                                break;
                            }
                        case 4:
                            if (checkEnemyInBounds(enemyNum, 0, 1) == true)
                            {
                                enemyCanMove = true;
                                break;
                            }
                            else
                            {
                                validDir[3] = 0;
                                break;
                            }
                        default:
                            std::cout << "\nError in enemy move function\n";
                            return;
                    }
                }
                else
                {
                    enemyCanMove = true;
                }
            }
        }
    }
}

bool DungeonCrawl::checkEnemyInBounds(int who, int row, int col)
{
    if ( enemyPos[0][who]+row >= 0 && enemyPos[0][who]+row < boardSize && enemyPos[1][who]+col >= 0 && enemyPos[1][who]+col < boardSize )
    {
        if (checkNewEnemyPos(who, row, col) == true)
        {
            return true;
        }
        else
            return false;
    }
    else
        return false;
}

bool DungeonCrawl::checkNewEnemyPos(int who, int row, int col)
{
    int oldEnemyRow {enemyPos[0][who]};
    int oldEnemyCol {enemyPos[1][who]};
    int newEnemyRow {oldEnemyRow+row};
    int newEnemyCol {oldEnemyCol+col};
    char newEnemyMeets = board[newEnemyRow][newEnemyCol];
    switch (newEnemyMeets)
    {
        case '-':
            board[oldEnemyRow][oldEnemyCol] = '-';
            board[newEnemyRow][newEnemyCol] = enemy;
            enemyPos[0][who] = newEnemyRow;
            enemyPos[1][who] = newEnemyCol;
            return true;
        case 'P':
            board[oldEnemyRow][oldEnemyCol] = '-';
            board[newEnemyRow][newEnemyCol] = 'e';
            gameOver = true;
            std::cout << "\nAn enemy hit you and you die. Better luck next time!\n";
            return true;
        case 'T':
            return false;
        case 'E':
            return false;
        case 'X':
            return false;
        default:
            return false;
    }
}

void DungeonCrawl::play()
{
    newGame();
    welcomMessage();
    while (!gameOver)
    {
        showBoard();
        playerMove();
        if (!gameOver)
        {
            enemyMove();
        }
    }
    showBoard();
}

main()
{
    bool keepPlaying {true};
    DungeonCrawl dCGame;

    while (keepPlaying)
    {
        dCGame.play();

        char playMore {};
        std::cout << "\n\nWould you like to keep playing? (y/n):";
        std::cin >> playMore;
        if (playMore == 'y' || playMore == 'Y')
            keepPlaying = true;
        else
            keepPlaying = false;
    }




    std::cin.clear();
    std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
    std::cin.get();
    return 0;
}