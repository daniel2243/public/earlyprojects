#include "Random.h"
#include <iostream>
#include <chrono>
#include <random>





int main()
{
    // make random number and have user guess it:
    std::uniform_int_distribution die100{ 1, 100 };
    int randNum { die100(Random::mt) };
    int userGuess {  };
    int userTries { 0 };
    std::cout << "Guess a whole number between 1 and 100:   ";
    while ( userGuess != randNum )
    {
        std::cin >> userGuess;
        if ( userGuess < randNum )
            std::cout << "\nToo low, try again: ";
        else if ( userGuess > randNum )
            std::cout << "\nToo high, try again: ";
        ++userTries;
    }
    std::cout << "That's the correct number!\n";
    std::cout << "You took " << userTries << " tries." << std::endl;




    // user makes random number and computer guesses:
    std::cout << "Now we swap, so you think of a number between 1 and 100 and the computer will guess: ";
    int compGuessMin { 1 };
    int compGuessMax { 100 };
    int userNum { };
    while ( userNum < compGuessMin || userNum > compGuessMax )
    {
        std::cin >> userNum;
        if ( userNum < compGuessMin || userNum > compGuessMax )
            std::cout << "\nThat's not in range, and we don't like cheaters >:( \nTry again: ";
    }

    int compGuess { };
    compGuess = Random::get( compGuessMin, compGuessMax );
    int compTries { 1 };
    while ( compGuess != userNum )
    {
        std::cout << "\nThe computer guesses " << compGuess << ". Should the computer guess higher(+) or lower(-)?: ";
        bool userLies { true };
        while ( userLies )
        {
            char askUserHL { };
            while ( askUserHL != '+' && askUserHL != '-' )
            {
                std::cin >> askUserHL;
                if ( askUserHL != '+' && askUserHL != '-' )
                std::cout << "\nThat's not one of the options, please try again: ";
            }
            if ( askUserHL == '+' && compGuess < userNum ) 
                {
                    compGuessMin = compGuess+1;
                    userLies = false;
                }
            else if ( askUserHL == '-' && compGuess > userNum )
                {
                    compGuessMax = compGuess-1;
                    userLies = false;
                }
            else
                std::cout << "This programmer doesn't think you are telling the truth, and we don't accept cheaters. Try again: ";
        }
        
        std::cout << "\nOk, the computer will try again!";
        compGuess = Random::get( compGuessMin, compGuessMax );
        ++compTries;
    }
    if ( compTries == 1 )
        std::cout << "\n Wow, the computer actually got it right on the first try, guessing " << compGuess << ". That's some crazy luck!" << std::endl;
    else
        std::cout << "\nThe computer guesses " << compGuess << "and got it right after " << compTries << " guesses. GG!" << std::endl;





    // computer guesses again, but cheats!
    std::cout << "Now it's time for the cheat round where the computer will get the right number in max 7 tries. \nEnter another number between 1 and 100: ";
    compGuessMin = 1;
    compGuessMax = 100;
    userNum = 0;
    while ( userNum < compGuessMin || userNum > compGuessMax )
    {
        std::cin >> userNum;
        if ( userNum < compGuessMin || userNum > compGuessMax )
            std::cout << "\nThat's not in range, and we don't like cheaters >:( \nTry again: ";
    }

    compGuess = ( compGuessMin + compGuessMax )/2 ;
    compTries = 1;
    while ( compGuess != userNum )
    {
        compGuess = ( compGuessMin + compGuessMax )/2 ;
        if(compGuess == userNum)
        {
            std::cout << "\nThe computer guesses " << compGuess << ".";
            break;
        }
        std::cout << "\nThe computer guesses " << compGuess << ". Should the computer guess higher(+) or lower(-)?: ";
        bool userLies { true };
        while ( userLies )
        {
            char askUserHL { };
            while ( askUserHL != '+' && askUserHL != '-' )
            {
                std::cin >> askUserHL;
                if ( askUserHL != '+' && askUserHL != '-' )
                std::cout << "\nThat's not one of the options, please try again: ";
            }
            if ( askUserHL == '+' && compGuess < userNum ) 
                {
                    compGuessMin = compGuess+1;
                    userLies = false;
                }
            else if ( askUserHL == '-' && compGuess > userNum )
                {
                    compGuessMax = compGuess-1;
                    userLies = false;
                }
            else
                std::cout << "This programmer doesn't think you are telling the truth, and we don't accept cheaters. Try again: ";
        }
        std::cout << "\nOk, the computer will try again!";
        ++compTries;
    }
    std::cout << "\nThe cheating computer got it in " << compTries << " tries. Told ya." << std::endl;

    
    

    system("pause");
    return 0;
}