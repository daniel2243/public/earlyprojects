#include <chrono>
#include <iostream>
#include <utility>

int main()
{
	const int maxPeople{10};
	int pancakesEaten[maxPeople][2]{}; //first index(0) is number of pancakes, second(1) is which person
	for (int person{0}; person < maxPeople; ++person)
	{
		std::cout << "Enter amount of pancakes person " << person+1 << " had for breakfast: ";
		std::cin >> pancakesEaten[person][0];
		pancakesEaten[person][1] = person+1;
	}
	std::cout << '\n';

    /*
	//checking mistakes
	for (int count {0}; count < maxPeople; ++count)
	{
		std::cout << "Person " << pancakesEaten[count][1] << " ate " << pancakesEaten[count][0] << " pancakes. \n";  
	}
	std::cout << '\n';
    */
	
	int mostPancakes{0};
	int whoAteMost{};
	for (int count {0}; count < maxPeople; ++count)
	{
		if (pancakesEaten[count][0] > mostPancakes)
		{
			mostPancakes = pancakesEaten[count][0];
			whoAteMost = pancakesEaten[count][1];
		}
	}
	std::cout << "Person " << whoAteMost << " ate the most pancakes - " << mostPancakes << "! \n\n";
	
	int leastPancakes{mostPancakes};
	int whoAteLeast;
	for (int count {0}; count < maxPeople; ++count)
	{
		if (pancakesEaten[count][0] < leastPancakes)
		{
			leastPancakes =pancakesEaten[count][0];
			whoAteLeast = pancakesEaten[count][1];
		}
	}
	std::cout << "Person " << whoAteLeast << " ate the least pancakes - " << leastPancakes << "! \n\n";
	
    /*
	//print unsorted arrays:
	std::cout << "Unsorted array of pancakes eaten:\n";
	for (int count {0}; count < maxPeople; ++count)
	{
		std::cout << "Person " << pancakesEaten[count][1] << " ate " << pancakesEaten[count][0] << " pancakes.\n";
	}
	std::cout << "\n\n";
    */
	
	//sorting ascending
	for (int startIndex {0}; startIndex < maxPeople; ++startIndex)
	{
		int smallestIndex {startIndex};
		for (int currentIndex {startIndex+1}; currentIndex < maxPeople; ++currentIndex)
		{
			if (pancakesEaten[currentIndex][0] < pancakesEaten[smallestIndex][0])
			{
				smallestIndex = currentIndex;
			}
		}
		for (int i {0}; i <= 1; ++i)
		{
			std::swap(pancakesEaten[startIndex][i], pancakesEaten[smallestIndex][i]);
		}
	}
	std::cout <<"\nArray sorted ascending:\n";
	for (int index {0}; index < maxPeople; ++index)
	{
		std::cout << "Person " << pancakesEaten[index][1] << " ate " << pancakesEaten[index][0] << " pancakes.\n";
	}

    //sorting descending
    for (int startIndex {0}; startIndex < maxPeople; ++startIndex)
    {
        int largestIndex {startIndex};
        for (int currentIndex {startIndex+1}; currentIndex < maxPeople; ++currentIndex)
        {
            if (pancakesEaten[currentIndex][0] > pancakesEaten[largestIndex][0])
            {
                largestIndex = currentIndex;
            }
        }
        for (int i {0}; i <=1; ++i)
        {
            std::swap(pancakesEaten[startIndex][i], pancakesEaten[largestIndex][i]);
        }
    }
    std::cout <<"\nArray sorted ascending:\n";
    for (int index {0}; index < maxPeople; ++index)
    {
        std::cout << "Person " << pancakesEaten[index][1] << " ate " << pancakesEaten[index][0] << " pancakes.\n";
    }

	std::cin.clear();
    std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
    std::cin.get();
	return 0;
}